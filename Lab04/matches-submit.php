<?php include "top.html"; ?>

<?php

    $name = $_GET["name"];
    
    /**
     * Trova l'elenco delle corrispondenze del nome passato
     * @param name - il nome utilizzato per trovare le corrispondenze
     * @return output - una serie di div contenenti le corrispondenze
     */

    function matches($name){
        $persone = file("singles.txt", FILE_IGNORE_NEW_LINES);
        $link = "http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/user.jpg";
        $output = "";

        $utente = get_utente_info($name, $persone);

        if($utente){
            foreach ($persone as $persona) {
                $persona = explode(",", $persona);
                
                if(compatibile($utente, $persona)){
                    $output .= formato_output($persona, $link);
                }
            }
        }

        return $output;
    }

    /**
     * Recupera tutte le informazioni dell'utente specificato
     * @param name - l'utente da cercare
     * @param persone - il pool delle persone in cui cercare
     * @return trovato - le informazioni dell'utente o nulla se non l'ho trovato
     */

    function get_utente_info($name, $persone){
        $trovato = null;

        for ($i = 0; $i < count($persone); $i++) {
            $corrente = explode(",", $persone[$i]);

            if($corrente[0] == $name){
                $trovato = $corrente;
            }

        }
        return $trovato;
    }

     /**
     * Restituisce il div formattato per una corrispondenza specifica
     * @param persona - la persona da inserire nel div
     */

    function formato_output($persona, $link){
        return "<p>
                    $persona[0]
                    <img src=\"$link\">
                </p>
                <ul>
                    <li>
                        <label><strong>gender:</strong>$persona[1]</label>
                    </li>
                    <li>
                        <label><strong>age:</strong>$persona[2]</label>
                    </li>
                    <li>
                        <label><strong>type:</strong>$persona[3]</label>
                    </li>
                    <li>
                        <label><strong>OS:</strong>$persona[4]</label>
                    </li>
                </ul>";
    }

    /**
     * Calcola se una persona è una corrispondenza valida per l'utente
     * @param utente - l'utente trovato
     * @param persona - la persona da controllare
     */

    function compatibile($utente, $persona){
        // lista elementi di una persona
        list($name, $gender, $age, $type, $os, $min, $max) = $persona;

        if($utente != null){
            if($utente[1] != $gender && ($utente[5] <= $age && $age <= $utente[6]) && $utente[4] == $os){
                for ($i=0; $i < 4; $i++) { // tipo di personalità
                    if($type[$i] == $utente[3][$i]){
                        return True;
                    }
                }
            }
        }
        return False;
    }

?>

<div>
    <strong>Matches for <?= $name ?></strong>
    <div class="match">
        <?= matches($name); ?>
    </div>
</div>

<?php include "bottom.html"; ?>