<?php include "top.html"; ?>

<div>
	<form action="signup-submit.php" method="post">
	    <fieldset>
	        <legend>New User Signup:</legend>
	        <div>
	            <strong>Name:</strong>
	            <input type="text" name="name" size="16">
	        </div>
	        <div>
	            <strong>Gender:</strong>
	            <label>Male<input type="radio" name="gender" value="male"></label>
	            <label>Female<input type="radio" name="gender" value="female" checked="checked"></label>
	        </div>
	        <div>
	            <strong>Age:</strong>
	            <input type="text" name="age" size="6" maxlength="2">
	        </div>
	        <div>
	            <strong>Personality type:</strong>
	            <input type="text" name="type" size="6" maxlength="4">
	            (<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp">Don't you know your type?</a>)
	        </div>
	        <div>
	        	<strong>Favorite OS:</strong>
	            <select name="OS">
	            	<option>Windows</option>
	            	<option>Mac OS</option>
	            	<option selected="selected">Linux</option>
	            </select>
	        </div>
	        <div>
	        	<strong>Seeking age:</strong>
	        	<label>
	        		<input type="text" name="min_age" size="6" maxlength="2" placeholder="min">
	        		to
	        		<input type="text" name="max_age" size="6" maxlength="2" placeholder="max">
	        	</label>
			</div>
			<div>
				<input type="submit" name="signup" value="Sign Up">
			</div>
	    </fieldset>
	</form>
</div>

<?php include "bottom.html"; ?>