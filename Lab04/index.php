<?php include "top.html"; ?>

<!--
	Name: Salvatore Costa
	Corso: A
	Data: 09-11-2020

	Questo è il file index.php per l'esercizio lab4.
	Consiste in un sito che permette all'utente di registrarsi e trovare la sua corrispondenza
	perfetta valutando delle specifiche
-->

<!-- MFN0634, Lab 04 (NerdLuv)
     This provided file is the front page that links to two of the files you are going
     to write, signup.php and matches.php.  You don't need to modify this file. -->
<div>
	<h1>Welcome!</h1>

	<ul>
		<li>
			<a href="signup.php">
				<img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/signup.gif" alt="icon" >
				Sign up for a new account
			</a>
		</li>
		
		<li>
			<a href="matches.php">
				<img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/heartbig.gif" alt="icon" >
				Check your matches
			</a>
		</li>
	</ul>
</div>

<?php include "bottom.html"; ?>
