<!--Salvatore Costa, Consegna laboratorio_03-->
<?php

//Selezione film
$movie = $_GET["film"];

// Informazioni generali
$info = file("$movie/info.txt",FILE_IGNORE_NEW_LINES); 
list($nome, $anno, $rating)= $info;

//Gerenal Overview
$overview_image_link = "$movie/overview.png";
$overview_file = file("$movie/overview.txt",FILE_IGNORE_NEW_LINES);

//Setting indirizzo
$indirizzo_base = "http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2";

// Setting img_title favicon 
$img_title = $rating >= 60 ? "$indirizzo_base/fresh.gif" : "$indirizzo_base/rotten.gif";

//Setting rotten e banner con indirizzo
$rating_image = $rating >= 60 ? "$indirizzo_base/freshbig.png" : "$indirizzo_base/rottenbig.png";
$rotten = "$indirizzo_base/rotten.gif"; // rotten utilizzato per le recensioni
$banner = "$indirizzo_base/banner.png";

// Opeazioni per il controllo subito dopo: per i due casi, cioè se ho più o meno di 10 recensioni nella cartella
// del film richiesto, e per effettuare la funzione controllo_recensioni
$recensioni = glob("$movie/review*.txt");
$tmp = count($recensioni);

// funzione per creare il vettore con solo 10 recensioni massimo nel caso in cui il film ne abbia di più                            
if($tmp > 10){
    function controllo_recensioni ($tmp){ 
        $b= NULL;
            for($i = 0; $i < 10; $i++){
                $b[$i] = $tmp;
            }
        return $b;
    }
    $numero_recensioni = controllo_recensioni($tmp);
    $num_rec = count($numero_recensioni);
}else{
    $num_rec = count($recensioni);
}

list($prima_colonna, $seconda_colonna) = get_columns($recensioni, $num_rec);

// Stampa la porzione di overview
function print_overview($testo_overview){
    $output = "<dl>"; // per apertura della lista con cui devo gestire gli elementi nell'overview
    foreach($testo_overview as $punti_definizione) {
        list($definizione, $descrizione) = explode(":", $punti_definizione);
        $output .= "<dt>$definizione</dt>
                        <dd>$descrizione</dd>";
    } // Apertura e chiusura della parola da definire e della definizione di essa
    $output .= "</dl>"; // Chiusura della lista con cui devo gestire gli elementi nell'overview
    return $output;
}

//Ritorna un array contenente i file review divisi in due colonne
function get_columns($recensioni, $num_rec){
    $meta = array_chunk($recensioni, ceil($num_rec / 2));
    $colonna[0] = $meta[0];
    $colonna[1] = NULL;
    if($num_rec > 1){
        $colonna[1] = $meta[1];
    }
    return $colonna;
}

// Stampa le recensioni
function print_recensioni($testo_recesione, $img, $rotten){
    $fresh = "$img/fresh.gif";
    $critici = "$img/critic.gif";

    $output = "";

    if (!is_null($testo_recesione)){ 
        foreach( $testo_recesione as $file_recensioni) {
            $blocco_recensioni = file("$file_recensioni", FILE_IGNORE_NEW_LINES);
            list($recensione, $rating_vote, $autore, $pubblicazione) = $blocco_recensioni;

            $review_image_link = $rating_vote == "FRESH" ? $fresh : $rotten;
            $output .= "<p class=\"citazioni bold overflow-hidden\">
                            <img src=\"$review_image_link\" alt=\"$rating_vote\">
                            <q>$recensione</q>
                        </p>
                        <p class=\"critici overflow-hidden\"> 
                            <img src=\"$critici\" alt=\"Critic\">
                            $autore<br>
                            $pubblicazione
                        </p>";
        } // Creare le recensioni come richiesto nel testo dell'esercizio
    }
    return $output;
}

?>

<!DOCTYPE html>

<html lang="en">

	<head>
		<title>Rancid Tomatoes</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href = "movie.css" type="text/css" rel="stylesheet">
        <link href="<?= $img_title ?>" type="image/gif" rel="shortcut icon">
	</head>

	<body>

		<div id = "banner" class="centred ripeti">
				<img src="<?= $banner ?>" alt="Rancid Tomatoes">
		</div>

		<h1 class="centred bold"> <?= "$nome ($anno)"?> </h1> <!--Titolo-->
		
			<div id = "area_testo_principale" class="overflow-hidden">
				<div id = "parte_destra" class= "colore">
					<div id = "overview">
						<img src= "<?= $overview_image_link ?>" alt="general overview">
					</div>
					<?= print_overview($overview_file); ?>
				</div>
			
				<div id = "parte_sinistra" class = "overflow-hidden">
					<div id = "rotten" class="ripeti">
						<img src="<?= $rating_image ?>" alt="Rotten"> 
							<span class = "bold" ><?= $rating ?>%</span>
					</div>

					<div class = "colonne">
                        <?= print_recensioni($prima_colonna, $indirizzo_base, $rotten); ?>
					</div>
					<div class = "colonne ">
                        <?= print_recensioni($seconda_colonna, $indirizzo_base, $rotten); ?>
					</div> <!--colonne-->
				</div> <!--parte_sinistra-->
				<p id = "numero_delle_recensioni" class="centred colore">
                    (1- <?= $num_rec ?>) of <?= $num_rec ?> </p>
            </div> <!--area_testo_principale-->

		<div id = "footer">
			<a href="http://validator.w3.org/check/referer">
				<img width = "88"
                src="https://upload.wikimedia.org/wikipedia/commons/b/bb/W3C_HTML5_certified.png "
                alt="Validate HTML 5"></a> <br>

			<a href="http://jigsaw.w3.org/css-validator/check/referer">
				<img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a>
		</div>

	</body>

</html>
