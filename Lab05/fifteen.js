var RIGHE = 4, COLONNE = 4;
var LARGHEZZA = 100, ALTEZZA = 100;
var TEGOLA_LIBERA = getString("class", 3, 3);

window.onload = function(){
    setPuzzle();
    document.getElementById("shufflebutton").onclick = shuffle;
}

function setPuzzle(){
    var celle = document.querySelectorAll("#puzzlearea div");
    var style = document.createElement("style");
    var end = false;

    for (var i = 0; i < RIGHE && !end; i++) {
        for (var j = 0; j < COLONNE; j++) {

            var y = i*LARGHEZZA;
            var x = j*ALTEZZA;

            var id = getString("id", i, j);
            var class_name = getString("class", i, j);

            style.innerHTML += `#${id} { background-position: ${-x}px ${-y}px; } \n`;
            style.innerHTML += `.${class_name} { top: ${y}px; left: ${x}px;} \n`;


            if((i+1) * (j+1) == RIGHE * COLONNE){ // Salta l'ultimo non elemento
                end = true;
                break;
            }
            
            celle[i*COLONNE + j].id = id;
            celle[i*COLONNE + j].classList.add(class_name);

            celle[i*COLONNE + j].onclick = function (){
                if(canMove(this)){
                    muovi_cella(this);
                    isWin();
                }
            };

            celle[i*COLONNE + j].onmouseover = function () {
                if(canMove(this)){
                    this.classList.add("movable");
                }
            };

            celle[i*COLONNE + j].onmouseleave = function () {
                if(canMove(this)){
                    this.classList.remove("movable");
                }
            };;
        }
    }

    document.getElementsByTagName('head')[0].appendChild(style);
}

function isWin(){
    var celle = document.querySelectorAll("#puzzlearea div");
    var end = false;
    for (var i = 0; i < RIGHE && !end; i++) {
        for (var j = 0; j < COLONNE; j++) {

            if((i+1) * (j+1) == RIGHE * COLONNE){ // Salta l'ultimo non elemento
                end = true;
                break;
            }

            if(celle[i*COLONNE + j].classList[0] != getString("class", i, j)) return false;
        }
    }

    alert("Win!");

    return true;
}

function getString(tipo, riga, colonna) {
    if(tipo == "id")
        return `tile_${riga}_${colonna}`;
    else
        return `pos_${riga}_${colonna}`;
}

function shuffle(){
    var movables =  getMovables();
   
    for (var i = 0; i < 50; i++) {
        muovi_cella(movables[random_index(0, movables.length)]);
        movables = getMovables();
    }
}

function random_index(min, max) {
    return parseInt(Math.random() * (max - min) + min);
}

function muovi_cella(cella){
    var tmp = TEGOLA_LIBERA;
    TEGOLA_LIBERA = cella.classList[0];
    cella.className = tmp;
}

function canMove(cella){
    var movables = getMovables();

    if(movables.includes(cella)) return true;
    else return false;
    
}

function getMovables(){
    var movables = new Array();
    var free = getPosition(TEGOLA_LIBERA);

    var riga = 1;
    var col = 0;

    for(var i = 0; i < 2; i++){
        if((free[i] + 1) <= 3){
            movables.push(
                document.getElementsByClassName(
                    getString("class", free[0] + riga, free[1] + col))[0]);
        }
    
        if((free[i] - 1) >= 0){
            movables.push(
                document.getElementsByClassName(
                    getString("class", free[0] - riga, free[1] - col))[0]);
        }

        [riga, col] = [col, riga];
    }

    return movables;
}

function getPosition(str){
    var splitted = str.split(/_/g).slice(1);
    splitted[0] = parseInt(splitted[0]);
    splitted[1] = parseInt(splitted[1]);
    return splitted;
}
